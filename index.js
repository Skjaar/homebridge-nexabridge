let Service, Characteristic;
let httpRequest = require('./httprequest.js');

module.exports = function(homebridge) {
    Service = homebridge.hap.Service;
    Characteristic = homebridge.hap.Characteristic;
    homebridge.registerAccessory("homebridge-nexabridge", "NexaBridge", NexaBridgeDevice);
};

function NexaBridgeDevice(log, config, timeToLive = 10000, delayTime = 70) {
    this.log = log;
    this.id = config['id'];
    this.deviceName = config['name'];
    this.deviceType = config['deviceType'];
    this.urlForDeviceStatus = `${config['url']}/v1/nodes?field=lastEvents&timeout=20000`;
    this.urlForDeviceControll = `${config['url']}/v1/nodes/${config['id']}/call?timeout=20000`;
    this.powerState = 100;
    this.log(`Configuration for ${this.id} ${config['name']} is completed.`);
    httpRequest.getInstance(timeToLive, this.id, config['delayTime'] ? config['delayTime'] : delayTime);
}

NexaBridgeDevice.prototype = {

    getSwitchOnCharacteristic: function(callback) {
        let that = this;
        httpRequest.get(this.urlForDeviceStatus, that.id).then(function(body) {
            for (scenarioElement in body) {
                if (body[scenarioElement].id === that.id) {
                    let lastEvent = body[scenarioElement].lastEvents;
                    return callback(null, that.getLastEvent(lastEvent, 'on'));
                }
            }
            that.log(`getSwitchOnCharacteristic: WARNING: Device ${that.id} was not found. Default value FALSE is returned.`);
            return callback(null, false);
        }, function(error) {
            that.log(`getSwitchOnCharacteristic: ERROR: ${error.message}`);
            callback(error);
        });

    },

    getDimmerOnCharacteristic: function(callback) {
        let that = this;
        httpRequest.get(this.urlForDeviceStatus, that.id).then(function(body) {
            for (scenarioElement in body) {
                if (body[scenarioElement].id === that.id) {
                    let lastEvent = body[scenarioElement].lastEvents;
                    return callback(null, that.getLastEvent(lastEvent, 'dimmerOn'));
                }
            }
            that.log(`getDimmerOnCharacteristic: WARNING: Device ${that.id} was not found. Default value FALSE is returned.`);
            return callback(null, false);
        }, function(error) {
            that.log(`getDimmerOnCharacteristic: ERROR: ${error.message}`);
            callback(error);
        });
    },

    getDimmerBrightnessCharacteristic: function(callback) {
        let that = this;
        httpRequest.get(this.urlForDeviceStatus, that.id).then(function(body) {
            let deviceCharacteristic = 0;
            for (scenarioElement in body) {
                if (body[scenarioElement].id === that.id) {
                    let lastEvent = body[scenarioElement].lastEvents;
                    return callback(null, that.getLastEvent(lastEvent, 'brightness'));
                }
            }
            that.log(`getSwitchBrightnessCharacteristic: WARNING: Device ${this.id} was not found. Default value 0 is returned.`);
            return callback(null, deviceCharacteristic);
        }, function(error) {
            that.log(`getSwitchBrightnessCharacteristic: ERROR: ${error.message}`);
            callback(error);
        });
    },

    setSwitchOnCharacteristic: function(powerState, callback) {
        let body = {
            method: powerState ? "turnOn" : "turnOff",
            cap: "switchBinary"
        };
        let that = this;
        httpRequest.post(that.urlForDeviceControll, body, that.id).then(function(body) {
            that.log("setSwitchOnCharacteristic: Setting device status to " + powerState);
            return callback(null);
        }, function(error) {
            that.log(`setSwitchOnCharacteristic: ERROR: ${error.message}`);
            callback(error);
        });
    },

    setDimmerOnCharacteristic: function(powerState, callback) {
        if (powerState === this.powerState) {
            return callback(null);
        }
        let number = Math.round(((this.powerState < 10 && this.powerState > 0 ? 10 : this.powerState) / 100) * 10) / 10;
        let body = {
            "method": "setValue",
            "cap": "switchLevel",
            "arguments": [number]
        };

        let that = this;
        httpRequest.post(that.urlForDeviceControll, body, that.id).then(function(body) {
            that.log("setDimmerOnCharacteristic: Setting device value to " + powerState);
            callback(null);
        }, function(error) {
            that.log(`setDimmerOnCharacteristic: ERROR: ${error.message}`);
            return callback(error);
        })
    },
    setDimmerBrightnessCharacteristic: function(powerState, callback) {
        let number = Math.round(((powerState < 10 && powerState > 0 ? 10 : powerState) / 100) * 10) / 10;
        let body = {
            "method": "setValue",
            "cap": "switchLevel",
            "arguments": [number]
        };
        let that = this;
        httpRequest.post(that.urlForDeviceControll, body, that.id).then(function(body) {
            that.log("setDimmerBrightnessCharacteristic: Setting device value to " + powerState);
            that.powerState = powerState;
            callback(null);
        }, function(error) {
            that.log(`setSwitchBrightnessCharacteristic: ERROR: ${error.message}`);
            return callback(error);
        });
    },

    getLastEvent: function(lastEvent, typeOfAction) {
        if (typeOfAction === 'dimmerOn') {
            if (Object.entries(lastEvent).length === 0 && lastEvent.constructor === Object) {
                this.powerState = 0;
                this.log(`Device ${this.id} has no last event, set to default status FALSE.`);
                return false;
            } else {
                this.powerState = lastEvent.switchLevel.value * 100;
                this.log(`Device ${this.id} has value ${this.powerState}`);
                return this.powerState > 0;
            }
        } else if (typeOfAction === 'brightness') {
            if (Object.entries(lastEvent).length === 0 && lastEvent.constructor === Object) {
                this.powerState = 50;
                this.log(`getSwitchBrightnessCharacteristic: Device ${this.id} has no last event. Set value to ${this.powerState}`);
                return this.powerState;
            } else {
                this.powerState = lastEvent.switchLevel.value * 100;
                this.log(`getSwitchBrightnessCharacteristic: Device ${this.id} has value ${this.powerState}`);
                return this.powerState;
            }
        } else if (typeOfAction === 'on') {
            if (Object.entries(lastEvent).length === 0 && lastEvent.constructor === Object || !lastEvent.switchBinary) {
                this.log(`getSwitchOnCharacteristic: Device ${this.id} has no last event, set to default status FALSE.`);
                return false;
            } else {
                let deviceValue = lastEvent.switchBinary.value;
                this.log(`getSwitchOnCharacteristic: Device ${this.id} has status ${deviceValue}`);
                return deviceValue;
            }
        }
    },

    getServices: function() {
        let informationService = new Service.AccessoryInformation();
        informationService
            .setCharacteristic(Characteristic.Manufacturer, "Robert Karlepalm")
            .setCharacteristic(Characteristic.Model, "My Nexa Plugin Model")
            .setCharacteristic(Characteristic.SerialNumber, "A very special number");

        this.log(`Mapping ${this.id} ${this.deviceType}`);
        switch (this.deviceType.toLowerCase()) {
            case 'dimmer':
                this.theService = new Service.Lightbulb(this.deviceName);
                this.theService
                    .getCharacteristic(Characteristic.On)
                    .on("get", this.getDimmerOnCharacteristic.bind(this))
                    .on("set", this.setDimmerOnCharacteristic.bind(this));
                this.theService
                    .addCharacteristic(new Characteristic.Brightness())
                    .on("get", this.getDimmerBrightnessCharacteristic.bind(this))
                    .on("set", this.setDimmerBrightnessCharacteristic.bind(this));
                break;
            case 'light':
                this.theService = new Service.Lightbulb(this.deviceName);
                this.theService.getCharacteristic(Characteristic.On)
                    .on('get', this.getSwitchOnCharacteristic.bind(this))
                    .on('set', this.setSwitchOnCharacteristic.bind(this));
                break;
            case 'switch':
                this.theService = new Service.Switch(this.deviceName);
                this.theService.getCharacteristic(Characteristic.On)
                    .on("get", this.getSwitchOnCharacteristic.bind(this))
                    .on("set", this.setSwitchOnCharacteristic.bind(this));
                break;
            default :
                throw "BIG FAIL! Could not find configured device to match predefinde device type. Only support 'dimmer', 'light' and 'switch'";
        }
        return [informationService, this.theService];
    }
};