let nexabridge = require('./index');
const nock = require('nock');   // https://github.com/nock/
require('mocha');               // https://mochajs.org/
require('should');              // https://shouldjs.github.io/

describe('Status', function() {
    describe('#get status', function() {
        it('should get status TRUE for a ON/OFF device', function(done) {
            const homebridge = getHomebridge(onOfDeviceConfig);

            const nockResponse = nock('http://10.0.1.200')
                .get('/v1/nodes?field=lastEvents&timeout=20000')
                .reply(200, deviceList);
            homebridge.nexa.getSwitchOnCharacteristic(function(error, theCallback) {
                nockResponse.isDone().should.be.true();
                theCallback.should.be.true();
                done();
            });
        });
        it('should get status FALSE for a ON/OFF device when last event is missing', function(done) {
            const homebridge = getHomebridge(missingEventForLightDevice);

            const nockResponse = nock('http://10.0.1.200')
                .get('/v1/nodes?field=lastEvents&timeout=20000')
                .reply(200, deviceList);
            homebridge.nexa.getSwitchOnCharacteristic(function(error, theCallback) {
                nockResponse.isDone().should.be.true();
                theCallback.should.be.false();
                done();
            });
        });
        it('should get FALSE with no device match', function(done) {
            const homebridge = getHomebridge(noDeviceFoundWithThisId);

            const nockResponse = nock('http://10.0.1.200')
                .get('/v1/nodes?field=lastEvents&timeout=20000')
                .reply(200, deviceList);
            homebridge.nexa.getSwitchOnCharacteristic(function(error, theCallback) {
                nockResponse.isDone().should.be.true();
                theCallback.should.be.false();
                done();
            });
        });
        it('should get value 50 for a dimmer device', function(done) {
            const homebridge = getHomebridge(dimmerDeviceConfig);

            const nockResponse = nock('http://10.0.1.200')
                .get('/v1/nodes?field=lastEvents&timeout=20000')
                .reply(200, deviceList);
            homebridge.nexa.getDimmerBrightnessCharacteristic(function(error, theCallback) {
                nockResponse.isDone().should.be.true();
                theCallback.should.be.eql(50);
                done();
            });
        });
        it('should get value 50 for a dimmer device with last event missing.', function(done) {
            const homebridge = getHomebridge(dimmerMissingInEvent);

            const nockResponse = nock('http://10.0.1.200')
                .get('/v1/nodes?field=lastEvents&timeout=20000')
                .reply(200, deviceList);
            homebridge.nexa.getDimmerBrightnessCharacteristic(function(error, theCallback) {
                nockResponse.isDone().should.be.true();
                theCallback.should.be.eql(50);
                done();
            });
        });
        it('should get status false for a dimmer device with value 0', function(done) {
            const homebridge = getHomebridge(dimmerWithZeroValueDeviceConfig);

            const nockResponse = nock('http://10.0.1.200')
                .get('/v1/nodes?field=lastEvents&timeout=20000')
                .reply(200, deviceList);
            homebridge.nexa.getDimmerOnCharacteristic(function(error, theCallback) {
                nockResponse.isDone().should.be.true();
                theCallback.should.be.eql(false);
                done();
            });
        });
        it('should get status false for a dimmer device with last event missing', function(done) {
            const homebridge = getHomebridge(dimmerMissingInEvent);

            const nockResponse = nock('http://10.0.1.200')
                .get('/v1/nodes?field=lastEvents&timeout=20000')
                .reply(200, deviceList);
            homebridge.nexa.getDimmerOnCharacteristic(function(error, theCallback) {
                nockResponse.isDone().should.be.true();
                theCallback.should.be.eql(false);
                done();
            });
        });
        it('should get status true for a dimmer device with value 0.5', function(done) {
            const homebridge = getHomebridge(dimmerDeviceConfig);

            const nockResponse = nock('http://10.0.1.200')
                .get('/v1/nodes?field=lastEvents&timeout=20000')
                .reply(200, deviceList);

            homebridge.nexa.getDimmerOnCharacteristic(function(error, theCallback) {
                nockResponse.isDone().should.be.true();
                theCallback.should.be.eql(true);
                done();
            });
        });
    });

    describe('#set status', function() {
        it('should set status to TRUE for a ON/OFF device', function(done) {
            const homebridge = getHomebridge(onOfDeviceConfig);

            const nockResponse = nock('http://10.0.1.200')
                .post('/v1/nodes/1016/call?timeout=20000', {
                    "method": "turnOn",
                    "cap": "switchBinary"
                })
                .reply(200, deviceControlResponse);
            const powerState = true;
            homebridge.nexa.setSwitchOnCharacteristic(powerState, function(error, theCallback) {
                nockResponse.isDone().should.be.true();
                done();
            });
        });

        it('should set status to 50 for a dimmer device', function(done) {
            const homebridge = getHomebridge(dimmerDeviceConfig);

            const nockResponse = nock('http://10.0.1.200')
                .post('/v1/nodes/1018/call?timeout=20000', {
                    "method": "setValue",
                    "cap": "switchLevel",
                    "arguments": [0.5]
                })
                .reply(200, deviceControlResponse);
            const powerState = 54;
            homebridge.nexa.setDimmerBrightnessCharacteristic(powerState, function(error, theCallback) {
                nockResponse.isDone().should.be.true();
                done();
            });
        });

        it('should set 100 with status true', function(done) {
            const homebridge = getHomebridge(dimmerDeviceConfig);

            const nockResponse = nock('http://10.0.1.200')
                .post('/v1/nodes/1018/call?timeout=20000', {
                    "method": "setValue",
                    "cap": "switchLevel",
                    "arguments": [1]
                })
                .reply(200, deviceControlResponse);
            const powerState = true;
            homebridge.nexa.setDimmerOnCharacteristic(powerState, function(error, theCallback) {
                nockResponse.isDone().should.be.true();
                done();
            });
        });
        it('should set 0 with status false', function(done) {
            const homebridge = getHomebridge(dimmerDeviceConfig);

            const nockResponse = nock('http://10.0.1.200')
                .post('/v1/nodes/1018/call?timeout=20000', {
                    "method": "setValue",
                    "cap": "switchLevel",
                    "arguments": [0]
                })
                .reply(200, deviceControlResponse);
            const powerState = false;
            homebridge.nexa.setDimmerOnCharacteristic(powerState, function(error, theCallback) {
                nockResponse.isDone().should.be.true();
                done();
            });

        });
    });

    describe('#error handling', function() {
        it('should use correct callback getSwitchOnCharacteristic', function(done) {
            const homebridge = getHomebridge(onOfDeviceConfig);

            const nockResponse = nock('http://10.0.1.200')
                .get('/v1/nodes?field=lastEvents&timeout=20000')
                .replyWithError({ message: "FAIL FAIL FAIL!!!!" });

            homebridge.nexa.getSwitchOnCharacteristic(function(error, theCallback) {
                nockResponse.isDone().should.be.true();
                error.message.should.be.eql("FAIL FAIL FAIL!!!!");
                done();
            });
        });
        it('should use correct callback getDimmerBrightnessCharacteristic', function(done) {
            const homebridge = getHomebridge(dimmerDeviceConfig);

            const nockResponse = nock('http://10.0.1.200')
                .get('/v1/nodes?field=lastEvents&timeout=20000')
                .replyWithError({ message: "FAIL FAIL FAIL!!!!" });

            homebridge.nexa.getDimmerBrightnessCharacteristic(function(error, theCallback) {
                nockResponse.isDone().should.be.true();
                error.message.should.be.eql("FAIL FAIL FAIL!!!!");
                done();
            });
        });
        it('should use correct callback getDimmerOnCharacteristic', function(done) {
            const homebridge = getHomebridge(dimmerDeviceConfig);

            const nockResponse = nock('http://10.0.1.200')
                .get('/v1/nodes?field=lastEvents&timeout=20000')
                .replyWithError({ message: "FAIL FAIL FAIL!!!!" });

            homebridge.nexa.getDimmerOnCharacteristic(function(error, theCallback) {
                nockResponse.isDone().should.be.true();
                error.message.should.be.eql("FAIL FAIL FAIL!!!!");
                done();
            });
        });
        it('should use correct callback setSwitchOnCharacteristic', function(done) {
            const homebridge = getHomebridge(onOfDeviceConfig);

            const nockResponse = nock('http://10.0.1.200')
                .post('/v1/nodes/1016/call?timeout=20000', {
                    "method": "turnOn",
                    "cap": "switchBinary"
                })
                .replyWithError({ message: "FAIL FAIL FAIL!!!!" });
            const powerState = true;
            homebridge.nexa.setSwitchOnCharacteristic(powerState, function(error, theCallback) {
                nockResponse.isDone().should.be.true();
                error.message.should.be.eql("FAIL FAIL FAIL!!!!");
                done();
            });
        });
        it('should use correct callback setDimmerBrightnessCharacteristic', function(done) {
            const homebridge = getHomebridge(dimmerDeviceConfig);

            const nockResponse = nock('http://10.0.1.200')
                .post('/v1/nodes/1018/call?timeout=20000', {
                    "method": "setValue",
                    "cap": "switchLevel",
                    "arguments": [0.5]
                })
                .replyWithError({ message: "FAIL FAIL FAIL!!!!" });
            const powerState = 54;
            homebridge.nexa.setDimmerBrightnessCharacteristic(powerState, function(error, theCallback) {
                nockResponse.isDone().should.be.true();
                error.message.should.be.eql("FAIL FAIL FAIL!!!!");
                done();
            });
        });
        it('should use correct callback setDimmerOnCharacteristic', function(done) {
            const homebridge = getHomebridge(dimmerDeviceConfig);

            const nockResponse = nock('http://10.0.1.200')
                .post('/v1/nodes/1018/call?timeout=20000', {
                    "method": "setValue",
                    "cap": "switchLevel",
                    "arguments": [0]
                })
                .replyWithError({ message: "FAIL FAIL FAIL!!!!" });
            const powerState = false;
            homebridge.nexa.setDimmerOnCharacteristic(powerState, function(error, theCallback) {
                nockResponse.isDone().should.be.true();
                error.message.should.be.eql("FAIL FAIL FAIL!!!!");
                done();
            });
        });
    });
});

function getHomebridge(config) {
    let homebridge = {};
    homebridge.hap = {};
    homebridge.hap.Service = {};
    homebridge.hap.Characteristic = {};
    homebridge.registerAccessory = function(a, b, myPlugin) {
        homebridge.nexa = new myPlugin(function(message) {
            console.log("[Logging through test] " + message);
        }, config, -1, 0);
    };
    nexabridge(homebridge);
    return homebridge;
}

const dimmerMissingInEvent = {
    "accessory": "NexaBridge",
    "name": "floor lamp",
    "url": "http://10.0.1.200",
    "deviceType": "dimmer",
    "id": 1020
};

const missingEventForLightDevice = {
    "accessory": "NexaBridge",
    "name": "A device with no event.",
    "url": "http://10.0.1.200",
    "deviceType": "light",
    "id": 1015
};

const onOfDeviceConfig = {
    "accessory": "NexaBridge",
    "name": "desk lamp",
    "url": "http://10.0.1.200",
    "deviceType": "light",
    "id": 1016
};

const noDeviceFoundWithThisId = {
    "accessory": "NexaBridge",
    "name": "Vägglampor",
    "url": "http://10.0.1.200",
    "deviceType": "light",
    "id": 1
};

const dimmerDeviceConfig = {
    "accessory": "NexaBridge",
    "name": "floor lamp",
    "url": "http://10.0.1.200",
    "deviceType": "dimmer",
    "id": 1018
};

const dimmerWithZeroValueDeviceConfig = {
    "accessory": "NexaBridge",
    "name": "floor lamp",
    "url": "http://10.0.1.200",
    "deviceType": "dimmer",
    "id": 1019
};

const deviceList = [
    {
        "id": 1015,
        "type": "NexaSwitch",
        "hideInApp": false,
        "name": "Some lamp 2",
        "roomId": 1,
        "groupNode": 0,
        "prio": 0,
        "favorite": true,
        "capabilities": [
            "switchBinary"
        ],
        "roomName": "Ute",
        "lastEvents": {}
    },
    {
        "id": 1016,
        "type": "NexaSwitch",
        "hideInApp": false,
        "name": "A Lamp",
        "roomId": 1,
        "groupNode": 0,
        "prio": 0,
        "favorite": true,
        "capabilities": [
            "switchBinary"
        ],
        "roomName": "Ute",
        "lastEvents": {
            "switchBinary": {
                "value": true,
                "sourceNode": 1016,
                "time": "2019-02-17T16:16:41+0100",
                "name": "switchBinary"
            },
            "methodCall": {
                "cap": "switchBinary",
                "targetNode": 1016,
                "method": "turnOn",
                "sourceNode": 1016,
                "time": "2019-02-17T15:46:39+0100",
                "name": "methodCall"
            }
        }
    },
    {
        "id": 1018,
        "type": "NexaDimmer",
        "hideInApp": false,
        "name": "A dimmed lamp",
        "roomId": 2,
        "groupNode": 0,
        "prio": 0,
        "favorite": false,
        "capabilities": [
            "switchLevel"
        ],
        "roomName": "Vardagsrum",
        "lastEvents": {
            "methodCall": {
                "cap": "switchLevel",
                "arguments": [
                    0.5
                ],
                "targetNode": 1018,
                "method": "setValue",
                "sourceNode": 1018,
                "time": "2019-03-05T17:53:20+0100",
                "name": "methodCall"
            },
            "switchLevel": {
                "value": 0.5,
                "isInt": false,
                "sourceNode": 1018,
                "time": "2019-03-10T20:19:00+0100",
                "name": "switchLevel"
            }
        }
    },
    {
        "id": 1019,
        "type": "NexaDimmer",
        "hideInApp": false,
        "name": "A dimmed lamp 2",
        "roomId": 2,
        "groupNode": 0,
        "prio": 0,
        "favorite": false,
        "capabilities": [
            "switchLevel"
        ],
        "roomName": "Vardagsrum",
        "lastEvents": {
            "methodCall": {
                "cap": "switchLevel",
                "arguments": [
                    0
                ],
                "targetNode": 1019,
                "method": "setValue",
                "sourceNode": 1019,
                "time": "2019-03-05T17:53:20+0100",
                "name": "methodCall"
            },
            "switchLevel": {
                "value": 0,
                "isInt": false,
                "sourceNode": 1019,
                "time": "2019-03-10T20:19:00+0100",
                "name": "switchLevel"
            }
        }
    },
    {
        "id": 1020,
        "type": "NexaDimmer",
        "hideInApp": false,
        "name": "A dimmed lamp 2",
        "roomId": 2,
        "groupNode": 0,
        "prio": 0,
        "favorite": false,
        "capabilities": [
            "switchLevel"
        ],
        "roomName": "Vardagsrum",
        "lastEvents": {}
    }
];

const deviceControlResponse = {
    "status": "OK"
};
