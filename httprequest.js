let request = require('request');

module.exports = new function() {

    let instance;
    let cacheData;
    let timeToLive;
    let expireTime;
    let delaySchema = [];
    let numberInOrder = 0;

    function createInstance(timeToLiveInput) {
        timeToLive = timeToLiveInput;
        expireTime = 0;
        cacheData = "";
        return {};
    }

    function wait(ms) {
        let start = new Date().getTime();
        let end = start;
        while (end < start + ms) {
            end = new Date().getTime();
        }
    }

    return {
        getInstance: function(timeToLiveInput, deviceId, delayTime) {
            if (!instance) {
                // console.log(`|| CREATING CACHE INSTANCE: time to live: ${timeToLiveInput}, delay: ${delayTime} ||`);
                instance = createInstance(timeToLiveInput);
            }
            delaySchema.push({ "id": deviceId, "delay": (delayTime * ++numberInOrder) });
            return instance;
        },

        get: function(theUrl, id) {
            return new Promise(function(resolve, reject) {
                for (device in delaySchema) {
                    if (delaySchema[device].id === id) {
                        wait(delaySchema[device].delay);
                    }
                }

                if (expireTime < new Date().getTime() || cacheData.length === 0) {
                    // console.log(`|| ${id} || CALLING SERVICE`);
                    request(
                        {
                            url: theUrl,
                            method: 'GET',
                            auth: {
                                user: 'nexa',
                                pass: 'nexa',
                                sendImmediately: false
                            },
                            json: true
                        },
                        function(error, response, body) {
                            if (error) {
                                // console.log(`|| ${id} || ERROR FROM SERVICE`);
                                expireTime = 0;
                                cacheData = "";
                                reject(error);
                            } else {
                                // console.log(`|| ${id} || READING FROM SERVICE`);
                                cacheData = body;
                                expireTime = new Date().getTime() + timeToLive;
                                resolve(body);
                            }
                        })
                } else {
                    // console.log(`|| ${id} || READING FROM CACHE`);
                    resolve(cacheData);
                }

            })
        },

        post: function(theUrl, body, id) {
            return new Promise(function(resolve, reject) {
                for (let device in delaySchema) {
                    if (delaySchema[device].id === id) {
                        wait(delaySchema[device].delay);
                    }
                }
                expireTime = 0;
                request(
                    {
                        url: theUrl,
                        method: 'POST',
                        body: body,
                        auth: {
                            user: 'nexa',
                            pass: 'nexa',
                            sendImmediately: false
                        },
                        json: true
                    },
                    function(error, response, body) {
                        if (error) {
                            reject(error);
                        } else {
                            resolve(body);
                        }
                    }
                )
            })
        }
    };
};