# NexaBridge
Nexabridge is a plugin for [Homebridge](https://github.com/nfarina/homebridge).  
The plugin communicates with your Nexabridge to enable Siri control over the devices connected to it. As of now scenarios and rooms configured in the Nexa Bridge are not supported.  

# Installation

    npm install homebridge-nexabridge -g

You might need to add a `sudo` if you har encountering and resistance.

## Issues
Report bugs [here](https://bitbucket.org/Skjaar/homebridge-nexabridge/issues?status=new&status=open) please. All feedback is welcome.

## config.json

    {
        "accessory": "NexaBridge",
        "name":"Bedroom light",
        "url" : "http://10.0.1.200",
        "deviceType" : "dimmer",
        "id": 1021
    }

## Configuration
        
|    Key     |                                                                                                            Value                                                                                                             |
|------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| accessory  | NexaBridge                                                                                                                                                                                                                   |
| name       | The name of the device. Best would be if you used the same name thats used in the NexaBridge. Easiest to know which device you are controlling.                                                                              |
| url        | ip number/url to your NexaBridge on local network                                                                                                                                                                            |
| id         | The id number for your device. Must be a switch, dimmer, scenario or a room.                                                                                                                                                 |
| deviceType | Supported device types are: `switch`: used for on and off devices. Switch icon will be used. `dimmer`: used for dimmer devices. `light`: Works with on and off but replaces the symbol with a lightbulb instead of a switch. |


## How to find the id for your device
Open up a browser and to to http://web.nexa.se/#/.
Enter your credentials and before you hit the Login button you want to open the "Inspector" (depending on browser) in your browser to be able to se the data trafic between the web page and the nexa server. 
Click on the "Network" tab and select the option that says XHR. Now login to the page.
You will see a list of calls to the nexa server. Find the one called "nodes" and view the response. You will identify your devices through the name and one you hace found your device in the list you have the id number as well.
 



